#include<stdio.h>
#include<math.h>
float root1(int a,float t1)
{
    float r1;
        r1=t1/(a*2);
        return r1;

}
float root2(int a,float t2)
{
    float r2;
        r2=t2/(a*2);
        return r2;
}
int main()
{
    int a,b,c,x,d,s;
    float r1,r2,t1,t2;
    printf("enter a,b,c\n");
    scanf("%d%d%d",&a,&b,&c);
    d=pow(b,2)-4*a*c;
    t1=-b+sqrt(d);
    t2=-b-sqrt(d);
    r1=root1(a,t1);
    r2=root2(a,t2);
    if (d>0)
    {
        s=1;
    }
    else if(d<0)
    {
        s=2;
    }
    else
    {
        s=3;
    }
    switch(s)
    {
        case 1 :printf("b^2-4*a*c=%d, hence the equation has real and unequal roots and %f and %f are the roots",d,r1,r2);
        break;
        case 2 :printf("b^2-4*a*c=%d, hence the equation has no real roots\n",d);
        break;
        case 3 :printf("b^2-4*a*c=%d, hence the equation has real and equal roots and %f is the root",d,r1);
        break;
        
    }

        return 0;
}
