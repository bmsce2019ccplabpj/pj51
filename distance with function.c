#include<stdio.h>
#include<math.h>
float distance(float p,float q,float r,float s)
{
	float d;
	d=sqrt(pow(r-p,2)+pow(s-q,2));
	return d;
}

int main()
{
	float p,q,r,s,dst;
	printf("enter the coordinates of two points x1,y1,x2,y2\n");
	scanf("%f%f%f%f",&p,&q,&r,&s);
	dst=distance(p,q,r,s);
	printf("distance between the points (%f,%f) and (%f,%f) is %f",p,q,r,s,dst);
	return 0;
}